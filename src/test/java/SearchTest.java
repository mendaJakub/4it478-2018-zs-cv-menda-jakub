import PageObjects.SearchResultPage;
import PageObjects.UpperMenu;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SearchTest extends BaseTest {
    WebDriver driver;
    private UpperMenu upperMenu;

    @Before
    public void testSetup(){
        // open browser
        driver = new FirefoxDriver();
        driver.get("https://www.amazon.com");
        // set up implicit wait timeout
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        this.upperMenu = new UpperMenu(driver);
    }

    @After
    public void testTeardown(){
        // close browser
        driver.close();
    }

    @Test
    public void simpleSearchTest() {
        // search for a product
        String searchText = "Ahoy";
        SearchResultPage searchResultsPage = upperMenu.search(searchText);

        // check search results
        Assert.assertThat(searchResultsPage.getResultsCountText(), containsString("results for \"" + searchText + "\""));
        Assert.assertThat(searchResultsPage.getSearchResultText(0), containsString(searchText));
    }

    @Test
    public void brandAmazonTest() {
        // search for a product
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Echo Dot");
        driver.findElement(By.cssSelector("form.nav-searchbar input[type='submit']")).click();

        // select Brand = Amazon
        driver.findElement(By.name("s-ref-checkbox-Amazon")).click();

        // verify first search item contains text "by Amazon"
        // WebDriverWait wait = new WebDriverWait(driver, 1);
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("s-item-container")));
        Assert.assertTrue(driver.findElement(By.className("s-item-container")).getText().contains("by Amazon"));
    }
}
