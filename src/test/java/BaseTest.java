import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import static utils.ScreenshotUtil.deleteOldReports;

public class BaseTest {
    protected WebDriver driver;

    @BeforeClass
    public static void TestSetup() {
        //set path to gecko driver
        WebDriverManager.firefoxdriver().setup();

        deleteOldReports();
    }
}
