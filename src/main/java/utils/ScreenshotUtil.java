package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotUtil {
    //Target path for screenshots
    private static final String SCREENSHOTS_TARGET_PATH = "Reports/";

    /**
     * Takes screenshot of the currently displayed browser window
     * @param driver webdriver to use for taking the screenshot
     * @return path to the created screenshot
     */
    public static String captureScreenshot(WebDriver driver) {
        try {
            TakesScreenshot ts = (TakesScreenshot)driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
            String filePath = SCREENSHOTS_TARGET_PATH + "Screenshot " + timeStamp + ".png";
            File destination = new File(filePath);
            FileUtils.copyFile(source, destination);
            return filePath;
        } catch (IOException e) {
            String errMsg = e.getMessage();
            System.err.println(errMsg);
            return errMsg;
        }
    }

    /**
     * Deletes old reports (screenshots) from Reports folder
     */
    public static void deleteOldReports() {
        try {
            FileUtils.cleanDirectory(new File(SCREENSHOTS_TARGET_PATH));
        } catch (Exception e) {
            //File permission problems are caught here.
            System.err.println("Could not delete old Reports from Directory: " + SCREENSHOTS_TARGET_PATH);
            System.err.println(e);
        }
    }



}
