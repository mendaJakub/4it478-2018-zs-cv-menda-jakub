package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetailPage extends BasePage {
    @FindBy(id="add-to-cart-button")
    private WebElement addToCartButton;

    @FindBy(css="h1")
    private WebElement confirmText;

    @FindBy(className="nav-cart-icon")
    private WebElement openCartButton;

    public ProductDetailPage(WebDriver driver) {
        super(driver);
    }

    public void addToCart() {
        addToCartButton.click();
    }

    public String getConfirmText() {
        return confirmText.getText();
    }

    public CartPage openCart() {
        openCartButton.click();
        return new CartPage(driver);
    }
}
