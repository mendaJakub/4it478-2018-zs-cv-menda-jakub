package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchResultPage extends BasePage {
    @FindBy(id="s-result-count")
    private WebElement searchResultsCount;

    @FindBy(css=".s-result-item h2")
    private List<WebElement> searchResultTitles;

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public String getResultsCountText() {
        return searchResultsCount.getText();
    }

    public String getSearchResultText(int index) {
        return searchResultTitles.get(index).getText();
    }

    public ProductDetailPage openSearchResult(int index) {
        searchResultTitles.get(index).click();
        return new ProductDetailPage(driver);
    }
}
