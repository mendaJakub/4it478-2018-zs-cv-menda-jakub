import PageObjects.CartPage;
import PageObjects.ProductDetailPage;
import PageObjects.SearchResultPage;
import PageObjects.UpperMenu;
import static utils.ScreenshotUtil.captureScreenshot;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;

public class AmazonOrderTest extends BaseTest {
    private UpperMenu upperMenu;
    private SearchResultPage searchResultPage;

    @Before
    public void testSetup(){
        // open browser
        driver = new FirefoxDriver();
        driver.get("https://www.amazon.com");
        // set up implicit wait timeout
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        this.upperMenu = new UpperMenu(driver);

        // search for a product
        searchResultPage = upperMenu.search("Teddy Bear");
    }

    @After
    public void testTeardown(){
        captureScreenshot(this.driver);
        // close browser
        driver.close();
    }

    @Test
    public void simpleOrderTest() {
        ProductDetailPage productDetailPage = searchResultPage.openSearchResult(0);
        productDetailPage.addToCart();
        Assert.assertEquals("Added to Cart", productDetailPage.getConfirmText());
        CartPage cartPage = productDetailPage.openCart();

        // verify product is present in the cart
        Assert.assertThat(cartPage.getProductTitle(), containsString("Teddy Bear"));
    }
}
