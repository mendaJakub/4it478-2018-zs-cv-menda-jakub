package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UpperMenu extends BasePage {
    @FindBy(id="twotabsearchtextbox")
    private WebElement searchField;

    @FindBy(css="form.nav-searchbar input[type='submit']")
    private WebElement searchBtn;

    public UpperMenu(WebDriver driver) {
        super(driver);
    }

    public SearchResultPage search(String searchText) {
        searchField.sendKeys(searchText);
        searchBtn.click();
        return new SearchResultPage(driver);
    }
}
